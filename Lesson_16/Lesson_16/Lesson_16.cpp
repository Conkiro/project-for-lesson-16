#include <iostream>
#include <ctime>
#include <Windows.h>

using namespace std;

// �������� ������� � ����� 16
int main()
{
    setlocale(LC_ALL, "Russian");

    int sum = 0;
    const int N = 5;
    int array[N][N] = { {} };

    //����� ������� ���� ������
    SYSTEMTIME st;
    GetSystemTime(&st);
    cout << "������� ����� ���������: " << st.wDay << endl;

    cout << "�������� N = 5" << endl;

    // ������� �� ������� ������� ���� �� N
    int task3 = (st.wDay % N);
    cout << "������� ������� ������� ���� �� N: " << task3 << endl;

    cout << "����� �������� �������:" << endl;
    // 1.���������� � 2.����� ������ �������
    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            array[i][j] = i + j;
            cout << array[i][j] << '\t';

            if (i == task3)
            {
                sum += array[task3][j];
            }
                                                
        }

        cout << '\n';
    }
       
    cout << "����� ��������� ������, ���������� �� ������� ������� ���� �� N = " << sum << '\n';
    
}